import { createRouter, createWebHistory } from 'vue-router'
import HomePage from '../views/HomeView/Home.vue'
import LoginPage from '../views/Login.vue'
import HotelResultsPage from '../views/HotelsView/HotelResults.vue'
import HotelDetailsPage from '../views/HotelDetailsView/HotelDetails.vue'
import CheckoutPage from '../views/CheckoutView/Checkout.vue'
import TripsPage from '../views/TripsView/MyTrips.vue'
import NotFoundPage from '../views/404View/NotFound.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'Home',
      component: HomePage,
    },
    {
      path: '/login',
      name: 'Login',
      component: LoginPage,
    },
    {
      path: '/hotelResults',
      name: 'Hotel Results',
      component: HotelResultsPage
    },
    {
      path: '/hotelDetails/:id',
      name: 'Hotel Details',
      component: HotelDetailsPage
    },
    {
      path: '/checkout/:id',
      name: 'Checkout',
      component: CheckoutPage
    },
    {
      path: '/mytrips',
      name: 'My Trips',
      component: TripsPage
    },
    {
      path: '/:notFound(.*)',
      name: 'Not Found',
      component: NotFoundPage
    },
  ]
})


export default router
