export const getHotelResultsFromLocalStorage =  () => {
    let hotelResults = localStorage.getItem('HotelResults')
    let parsedHotelResults = JSON.parse(hotelResults)
    return parsedHotelResults
}

export const setMetaDataToLocalStorage = (meta) => {
    let metaResults = localStorage.setItem('MetaResults', meta)
    let stringifiedMetaResults = JSON.stringify(metaResults)
    return stringifiedMetaResults
}

export const getHotelMetaDataFromLocalStorage = () => {
    let metaResults = localStorage.getItem('MetaResults')
    let parsedMetaResults = JSON.parse(metaResults)
    return parsedMetaResults
}

export const isHotelResultsRetrieved = () => {
    if(localStorage.getItem('HotelResults')) return true
    else return false
}

export const getSearchFields = () => {
    let searchFieldsValue = localStorage.getItem('SearchFieldsValue')
    let parsedFieldsValue = JSON.parse(searchFieldsValue)
    return parsedFieldsValue
}

